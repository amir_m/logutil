/*
* void GetLogs( std::vector<LogFile>&, const std::string& ); 
*
* DESCRIPTION
*  Creates a variable of type LogFile for each .log file from the 
*  given directory and stores it in the given vector
*  Note: the saved filenames are of the form <passed path> + <filename>.log
*
* PARAMETRS
*  std::vector<LogFile>& - container where LogFile variables will be stored
*  const string& - a string with the directory in which to search
*/
void GetLogs( std::vector<LogFile>&, const std::string& ); 
