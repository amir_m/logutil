/*
* This file contains the declaration of the LogParser class.  
*/
#include "LogFile.h"

/*                
*  This class is a functional object for flexible parsing of log files.
*/
class LogParser
{
    private:
        /***********************************************************************************************/
        /* These variables are used to parse the file in parts */
        /* (save intermediate information between calls to the BufferParsing function) */
        LogFile::EntryType type;   // Used to iterate over types
        bool skipStartPart;        // Specifies that we can skip characters before "~#" at the beginning
        bool tildaWasLast;         // True, when the last part ended with "~"
        int k;                     // Is used to iterate over characters in the template of the
                                   //   substring being searched for ( see the BufferParsing method)
        /***********************************************************************************************/
        static const char* TYPES[ NUMBER_OF_TYPES ]; // type names in symbolic form (search substrings)

    public:
        enum SearchFlags
        {
            NORMAL_SEARCH,
            BACK_SEARCH
        };
        LogParser();
        static bool CheckLogFile(const std::vector<char>& buf, uint32_t);
        static std::string SearchingOfTime(const std::vector<char>&, uint32_t, uint32_t);
        void BufferParsing( const std::vector<char>& buf, uint32_t len, LogFile& log );
        void operator()( LogFile& log );
        static uint32_t nextMarkPosition(  
                                    const std::vector<char>& buf,
                                    uint32_t, 
                                    uint32_t, 
                                    enum SearchFlags = NORMAL_SEARCH
                                 ); 
};