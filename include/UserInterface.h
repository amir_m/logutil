/*
* This file contains the declaration of the LogFile class.  
*/

#include <vector>
#include <string>
#include "../include/LogFile.h"

/*
*  Class UserInterface provides interaction with the user
*  and stores the parameters entered by him
*/
class UserInterface
{
    private:
        bool OneFileMode, problems, exitPrg;
        std::string directory, filename;

    public:
        UserInterface();
        UserInterface( int argc, char** argv);

        void displayHelpMenu() const;
        void displayMessage( const std::string& msg, bool addLineBreak = true ) const;
        void displayErrorMessage( const std::string& msg) const;
        void displayResult( const std::vector< LogFile >& LogList, const LogFile& TotalInfo ) const;
        void displayNumberOfEntries( const LogFile& log ) const;
        void displayLogInfo( const LogFile& log ) const;
        void setProblems();
        const std::string& getDirectory() const;
        const std::string& getFilename() const; 
        const bool& isOneFileMode() const;    
        bool needExitPrg() const;
}; 
