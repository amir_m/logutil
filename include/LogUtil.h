// main LogUtil header
// contain global constants and common types
#include <string>
#include <cstdint>

const uint32_t BUFFER_SIZE = 40*1024*1024;      // size of input buffer in bytes

// declaration of exception
class ERROR 
{
    private:
        std::string ErrorName;
    public:
        ERROR(const std::string& s): ErrorName(s) {};
        const std::string& name() const { return ErrorName; }
};
