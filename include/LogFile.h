/*
* This file contains the declaration of the LogFile class.  
*/

#include <string>
#define NUMBER_OF_TYPES 5  // number of entrieses types

#ifndef H_LOGFILE
#define H_LOGFILE

/*
*   This class is designed to store a set of data associated with a specific log file.
*/

typedef uint32_t counter_t;

class LogFile
{
    private:
        counter_t EntriesCounter[NUMBER_OF_TYPES];
        const std::string filename;
        std::string timeOfFirstEntry, timeOfLastEntry;
        bool correctness;

    public:

        enum EntryType
        {               
            TRACE = 0, 
            INFO  = 1, 
            DEBUG = 2, 
            WARN  = 3, 
            ERROR = 4
        };

        counter_t& operator[] (EntryType i);
        const counter_t& operator[] (EntryType i) const; // need if we need only read
        const LogFile& operator+= ( const LogFile& log );

        LogFile() = delete; // you must specify the file name when calling the constructor
        LogFile( const std::string& name );

        void setTimeOfFirstEntry( const std::string& s );
        void setTimeOfLastEntry( const std::string& s );
        void setCorrectness( bool value );

        bool isCorrect() const;
        bool needExitPrg() const;
        const std::string& getTimeOfFirstEntry() const;
        const std::string& getTimeOfLastEntry() const;
        const std::string& getFilename() const;
};

#endif
