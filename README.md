The utility processes all files in the current directory.
To select a directory manually, you need to run the utility with the -d key, 
and then specify the path to the directory.
To process one file, you need to run the utility with the -e key, 
then specify full filename of the file.
To get help menu, you need to run the utility with the -h key.
Note: The utility works only with files whose entries are marked with ~# symbols.
      Files without the specified date will be considered invalid.
