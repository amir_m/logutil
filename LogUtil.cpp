/*
*   It is main file of LogUtil
*/
#include <vector>
#include <algorithm>
#include <thread>
#include <atomic> 
#include <mutex>
#include <condition_variable>
#include <memory>
#include "include/UserInterface.h"
#include "include/GetLogs.h"
#include "include/LogUtil.h"
#include "include/LogParser.h"

int main( int argc, char *argv[] )
{     
    UserInterface interface( argc, argv );
    LogFile totalNumberOfEntries("Not a file");  // for general count (not associated with any file)
    std::vector<LogFile> Logs;                     
    std::vector<LogFile>::iterator logfile;
    std::atomic<uint32_t> indexOfNextLog(0);     // Logs[indexOfNextLog] - next unparsed log 
    std::vector< std::thread > Threads( std::thread::hardware_concurrency() );
    std::mutex mut;

    // if user input is incorrect or user just requested help mode we need exit program
    if( interface.needExitPrg() ) return 0;


    if( !interface.isOneFileMode() )
    {
        // save filenames from the directory in fileNames
        GetLogs( Logs, interface.getDirectory() );    
    }
    else
    {
        // if chekOnlyOneFile is true, the name of a single file is stored in second argument
        Logs.push_back( std::move( LogFile(interface.getFilename()) ) ); 
    }
    

    for( auto t = Threads.begin(); t != Threads.end(); t++ )
    {
        *t =  
        std::thread( 
                    [
                     &interface,
                     &totalNumberOfEntries,
                     &mut,
                     &Logs,
                     &indexOfNextLog
                    ]() 
                    {
                        while( true )
                        {

                            uint32_t index;
                            {
                                std::lock_guard<std::mutex> lk(mut);
                                if( indexOfNextLog < Logs.size() )
                                {
                                    index = indexOfNextLog;
                                    indexOfNextLog++;
                                }
                                else
                                {
                                    return;  // no files for parsing
                                }
                            } 
                             
                            // parse current log by logParsing and and store the exit status in errorCode
                            try
                            {
                                LogParser parser;
                                parser( Logs[index] );
                                std::lock_guard<std::mutex> lk(mut);
                                // add the counter for the current file to the total counter
                                totalNumberOfEntries += Logs[index];
                            } 
                            catch( ERROR err )
                            {
                                std::lock_guard<std::mutex> lk(mut);
                                interface.displayErrorMessage( err.name() );
                                // the interface must be aware of the problem in order to issue a warning
                                interface.setProblems(); 
                            }
                        }
                    }
                ); 
    };

    // wait threads if they still working
    for_each( 
              Threads.begin(), 
              Threads.end(),  
              [] ( std::thread& t ) { if( t.joinable() ) t.join(); } 
            );

    interface.displayResult( Logs, totalNumberOfEntries );

    return 0; 
}
