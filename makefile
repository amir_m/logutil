############################ Release Version ######################################

LogUtil: SearchingOfTime.o UserInterface.o LogFile.o LogParser.o BufferParsing.o CheckLogFile.o NextMarkPosition.o GetLogs.o LogUtil.o makefile
	g++ -O3 -Wall LogFile.o UserInterface.o LogUtil.o CheckLogFile.o LogParser.o BufferParsing.o SearchingOfTime.o GetLogs.o NextMarkPosition.o -o LogUtil -lstdc++fs

LogFile.o: ./src/LogFile.cpp ./include/LogFile.h
	g++ -O3 -Wall -c ./src/LogFile.cpp

SearchingOfTime.o: ./src/SearchingOfTime.cpp ./include/LogParser.h makefile
	g++ -O3 -Wall -c ./src/SearchingOfTime.cpp

BufferParsing.o: ./src/BufferParsing.cpp ./include/LogParser.h ./include/LogFile.h makefile
	g++ -O3 -Wall -c ./src/BufferParsing.cpp

CheckLogFile.o: ./src/CheckLogFile.cpp ./include/LogParser.h makefile
	g++ -O3 -Wall -c ./src/CheckLogFile.cpp

NextMarkPosition.o: ./src/NextMarkPosition.cpp ./include/LogParser.h makefile
	g++ -O3 -Wall -c ./src/NextMarkPosition.cpp

LogParser.o: ./src/LogParser.cpp ./include/LogFile.h ./include/LogUtil.h ./include/LogParser.h makefile 
	g++ -O3 -Wall -c ./src/LogParser.cpp

GetLogs.o: ./src/GetLogs.cpp ./include/GetLogs.h ./include/LogFile.h makefile
	g++ -O3 -std=c++17 -Wall -c ./src/GetLogs.cpp

UserInterface.o: ./src/UserInterface.cpp ./include/UserInterface.h
	g++ -O3 -Wall -c ./src/UserInterface.cpp

LogUtil.o: LogUtil.cpp ./include/UserInterface.h ./include/LogFile.h ./include/GetLogs.h ./include/LogUtil.h makefile
	g++ -O3 -Wall -c LogUtil.cpp

liblogutil.a: UserInterface.o GetLogs.o LogParser.o NextMarkPosition.o CheckLogFile.o BufferParsing.o SearchingOfTime.o LogFile.o
	ar -rcs liblogutil.a UserInterface.o GetLogs.o LogParser.o NextMarkPosition.o CheckLogFile.o BufferParsing.o SearchingOfTime.o LogFile.o

############################ Debugging Version ######################################

LogUtil_d: SearchingOfTime_d.o UserInterface_d.o LogFile_d.o LogParser_d.o BufferParsing_d.o CheckLogFile_d.o NextMarkPosition_d.o GetLogs_d.o LogUtil_d.o makefile
	g++ -Wall -g LogFile_d.o UserInterface_d.o LogUtil_d.o CheckLogFile_d.o LogParser_d.o BufferParsing_d.o SearchingOfTime_d.o GetLogs_d.o NextMarkPosition_d.o -o LogUtil_d -lstdc++fs

LogFile_d.o: ./src/LogFile.cpp ./include/LogFile.h
	g++ -g -c -Wall ./src/LogFile.cpp -o LogFile_d.o

SearchingOfTime_d.o: ./src/SearchingOfTime.cpp ./include/LogParser.h makefile
	g++ -g -c -Wall ./src/SearchingOfTime.cpp -o SearchingOfTime_d.o

BufferParsing_d.o: ./src/BufferParsing.cpp ./include/LogParser.h ./include/LogFile.h makefile
	g++ -g -c -Wall ./src/BufferParsing.cpp -o BufferParsing_d.o

CheckLogFile_d.o: ./src/CheckLogFile.cpp ./include/LogParser.h makefile
	g++ -g -c -Wall ./src/CheckLogFile.cpp -o CheckLogFile_d.o

NextMarkPosition_d.o: ./src/NextMarkPosition.cpp ./include/LogParser.h makefile
	g++ -g -c -Wall ./src/NextMarkPosition.cpp -o NextMarkPosition_d.o

LogParser_d.o: ./src/LogParser.cpp ./include/LogFile.h ./include/LogUtil.h ./include/LogParser.h makefile 
	g++ -g -c -Wall ./src/LogParser.cpp -o LogParser_d.o

GetLogs_d.o: ./src/GetLogs.cpp ./include/GetLogs.h ./include/LogFile.h makefile
	g++ -g -c -std=c++17 -Wall ./src/GetLogs.cpp -o GetLogs_d.o

UserInterface_d.o: ./src/UserInterface.cpp ./include/UserInterface.h
	g++ -g -c -Wall ./src/UserInterface.cpp -o UserInterface_d.o

LogUtil_d.o: LogUtil.cpp ./include/UserInterface.h ./include/LogFile.h ./include/GetLogs.h ./include/LogUtil.h makefile
	g++ -g -c -Wall LogUtil.cpp -o LogUtil_d.o

clean:
	rm *.o LogUtil LogUtil_d