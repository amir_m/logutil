/*
*   This file contains the definition of the LogParser constructor and operator ()
*/

#include <fstream>
#include <string>
#include <vector>
#include "../include/LogUtil.h"
#include "../include/LogParser.h"

LogParser::LogParser():
        type( LogFile::EntryType::TRACE ), 
        skipStartPart( true ),
        tildaWasLast( false ),
        k(0)
{}

const char* LogParser::TYPES[ NUMBER_OF_TYPES ] =       // contains the names of entrieses types
                                {
                                 "TRACE", 
                                 "INFO", 
                                 "DEBUG", 
                                 "WARN", 
                                 "ERROR"
                                }; 

/*  
*  Сhecks the log file for correctness and then
*  counts the number of records of each type and finds
*  the date and time of the first and last log entries.
*  The found data is stored in the passed LogFile variable.
*/
void LogParser::operator()( LogFile& log )
{

    // open file
    std::ifstream logFile( log.getFilename().c_str() );
    if( !logFile.is_open() )
    {
        log.setCorrectness( false );
        throw ERROR("no such file");
        return;
    }

    // create an input buffer
    std::vector<char> buf( BUFFER_SIZE );
    
    // reading and parsing of first part 
    logFile.read( &buf[0] , BUFFER_SIZE );   
    if( !CheckLogFile(buf, logFile.gcount()) ) // check the file for correctness
    {
        log.setCorrectness( false );
        throw ERROR("incorrect file: " + log.getFilename());
        return;
    }   
    BufferParsing(buf, logFile.gcount(), log);
    
    // search the date and time of the first entry
    log.setTimeOfFirstEntry(
        SearchingOfTime( 
                        buf,
                        logFile.gcount(), 
                        nextMarkPosition(buf, logFile.gcount(), 0)
                    )
    );

    // read other parts
    while( !logFile.eof() )
    {
        logFile.read(&buf[0], BUFFER_SIZE);
        BufferParsing(buf, logFile.gcount(), log);
    }

    // search the date and time of the last entry
    log.setTimeOfLastEntry(
        SearchingOfTime( 
                        buf,
                        logFile.gcount(), 
                        nextMarkPosition(buf, logFile.gcount(), logFile.gcount() - 1, BACK_SEARCH)
                    )
    );

    logFile.close();
}




