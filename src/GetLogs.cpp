/*
*   This file contains the definition of the function FetFilenames
*/
#include <experimental/filesystem>
#include <algorithm>
#include <vector>
#include <string>
#include "../include/LogFile.h"

namespace fs = std::experimental::filesystem;

//  Creates a variable of type LogFile for each .log file from the 
//  given directory and stores it in the given vector
//  Note: the saved filenames are of the form <passed path> + <filename>.log
void GetLogs( std::vector<LogFile>& logs, const std::string& path)
{
    auto it = fs::directory_iterator(path);
    const std::string extension = ".log";
    for( auto j = fs::begin(it); j != fs::end(it); j++ )
    {
        const std::string &s = j->path().string();
        int i = s.length() - 1;
        if( ( s.substr(i - 3, i) == extension ) && fs::is_regular_file(*j) )
        {
            logs.push_back( std::move(LogFile(s)) );
        }
    }
} 
