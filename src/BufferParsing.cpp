/*
*   This file contains the definition of the method BufferParsing of class LogParser
*/
#include <vector>
#include "../include/LogUtil.h"
#include "../include/LogParser.h"

/*
* void LogParser::BufferParsing( char *buf, uint32_t len, LogFile& log ) 
*
* DESCRIPTION
*   This method is looking for names of entrieses types in passed buffer
*   and saves the count of entrieses of each type in passed LogFile variable.
*   With this method, you can parse the log file 
*   into parts of arbitrary length (min. 1 byte), sequentially passing it pieces of the file.
*
* PARAMETRS
*   const std::vector<char>& buf- buffer link
*   unsigned int - size of buffer
*   LogFile& - LogFile variable associated with file
*/
void LogParser::BufferParsing(  
                                const std::vector<char>& buf, 
                                uint32_t len, 
                                LogFile& log
                             )
{                        
    
    unsigned int position = 0;

    // skip characters up to the first '~#'
    if( ( skipStartPart ) && ( (buf[0] != '#') || (!tildaWasLast) ) ) 
        position = nextMarkPosition(buf, len, 0) + 2;

    skipStartPart = false; 
    tildaWasLast = ( buf[len-1] == '~' ) ? true : false;

    for( unsigned int currentChar = position; currentChar < len; currentChar++ )
    {   
        // This condition checks if function can run acceleration for parsing
        // when variable k is 0, we can ignore all characters out of range ['A'..'Z']
        // (instead of the cumbersome substring matching for each record type from TYPES).
        // If k is not zero then we can't ignore other characters, 
        // due to the peculiarities of log file processing in parts.
        if( k == 0 )
        {
            // for most typical log characters, the first condition will fail and 
            // the second will not be checked (feature of the && operator)
            // that means we will get only one check for most characters
            while( !( ('A' <= buf[currentChar]) && (buf[currentChar] <= 'Z') )  )
            {
                currentChar++;
                if( currentChar == len ) return;
            }
        }

        while( type <= LogFile::EntryType::ERROR )
        {
            
            while( buf[currentChar] == TYPES[type][k] )
            {
                k++; 
                currentChar++;
                if( currentChar >= len ) return; // we can break parsing thanks to static variables
            }
                        
            if( (TYPES[type][k] == '\0' ) || ( currentChar == (len - 1) ) )
            {
                // if the string is found, then increase the corresponding counter
                log[type]++;
                // skip everything up to and including "~#"
                currentChar = nextMarkPosition(buf, len, currentChar);
                if( currentChar == len )    // if this is the end, then on the next call, 
                    skipStartPart = true;   // you need to finish skipping characters until the next '~#'
                k = 0;
                break;
            }

            k = 0;
            type = static_cast<LogFile::EntryType>(type + 1);
        }
        type = LogFile::EntryType::TRACE;
    }
}
