/*
*   This file contains the definition of the method NextMarkPosition of class LogParser
*/
#include <cstdint>
#include <vector>
#include "../include/LogParser.h"

/*
* uint32_t nextMarkPosition(std::vector<char>&, uint32_t, uint32_t, bool); 
*
* DESCRIPTION
*   Returns the position of the next marker (~#) in the buffer, 
*       starting at the given index.
*   Supports searching backwards (from the given index to the beginning). 
*   To activate the search in the opposite direction, 
*       you must pass an additional argument BACK_SEARCH (from enum searchFlags).
*   If the marker is not found, returns the index past the last element of the buffer.
*
* PARAMETRS
*   std::vector<char>& - buffer link
*   uint32_t - buffer size
*   uint32_t - start position
*   enum SearchFlags - (optional) search mode switch,
*       value NORMAL_SEARCH (default) - from positon to end;
*       value BACK_SEARCH  - from position to start
*
* RETURN VALUE
*   uint32_t - positon of next marker (~#) 
*   or the index past the last element (length variable value)
*/
uint32_t LogParser::nextMarkPosition(
                                const std::vector<char>& buf, 
                                uint32_t len, 
                                uint32_t position, 
                                enum SearchFlags searchMode
                            )
{
    if( searchMode == BACK_SEARCH )
    {
        for( uint32_t i = position; i >= 0; i-- )
        {
            if( ( buf[i] == '~' ) && (buf[i+1] == '#') ) return i;
        }
        return len;  // marker was not found
    }

    for( uint32_t i = position; i < len - 1; i++ )
    {
        if( ( buf[i] == '~' ) && (buf[i+1] == '#') ) return i;
    }
    return len;     // marker was not found
}
