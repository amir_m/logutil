/*
*   This file contains implementation of LogFile methods
*/
#include "../include/LogFile.h"

counter_t& LogFile::operator[] (EntryType i) { return EntriesCounter[i]; }
const counter_t& LogFile::operator[] (EntryType i) const { return EntriesCounter[i]; }

const LogFile& LogFile::operator+= ( const LogFile& log )
{
    for( 
        EntryType type = EntryType::TRACE; 
        type <= EntryType::ERROR; 
        type = static_cast<EntryType>(type + 1)
    )
    {
        EntriesCounter[type] += log[type];
    }
    return *this;
}

LogFile::LogFile( const std::string& name ): filename(name), correctness(true)
{
    for( 
        EntryType i = TRACE; 
        i <= ERROR; 
        i = static_cast<EntryType>(i + 1)
       ) 
    {
        EntriesCounter[i] = 0;
    }
};

bool LogFile::isCorrect() const { return correctness; }
void LogFile::setTimeOfFirstEntry( const std::string& s ) { timeOfFirstEntry = s; }
void LogFile::setTimeOfLastEntry( const std::string& s ) { timeOfLastEntry = s; }
void LogFile::setCorrectness( bool value ) { correctness = value; }

const std::string& LogFile::getTimeOfFirstEntry() const { return timeOfFirstEntry; }
const std::string& LogFile::getTimeOfLastEntry() const { return timeOfLastEntry; }
const std::string& LogFile::getFilename() const { return filename; }
