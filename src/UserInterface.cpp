/*
*   This file contains implementation of LogFile methods
*/

#include <iostream>
#include <string>
#include <vector>
#include "../include/UserInterface.h"

UserInterface::UserInterface(): 
                                OneFileMode(false),
                                problems(false)
{};

UserInterface::UserInterface( int argc, char** argv ): 
                                                        OneFileMode(false),
                                                        problems(false),
                                                        exitPrg(false)
{
    directory = ".";
    // processing command line options
    if( argc > 1 )
    {
        if( ( argv[1][0] == '-' ) && ( argv[1][2] == '\0' ) )
        {
            switch( argv[1][1] )
            {
                case 'd':    // the -d key specifies a directory with log files
                    directory = argv[2];
                    break;
                case 'e':    // the -e key allows to check only one file
                    OneFileMode = true;
                    filename = argv[2];
                    break;
                case 'h':
                    displayHelpMenu();
                    exitPrg = true;
                    break;
                default:
                    std::cout << "Wrong key, use -h for help\n";
                    exitPrg = true;
            }
        }
        else
        {
            std::cout << "Invalid argumets, use -h for help\n";
            exitPrg = true;
        }
    }
}

// tells the calling program whether to terminate
bool UserInterface::needExitPrg() const { return exitPrg; }

// show help information to user
void UserInterface::displayHelpMenu() const
{                     
    std::cout << "Use util without keys to parse current directory\n";
    std::cout << "Use ' -d [NAME_OF_DIRECTORY] ' to parse another directory\n";
    std::cout << "Use ' -e [FULL_FILENAME] ' to parse one file\n"; 
}

// show message to user 
void UserInterface::displayMessage( const std::string& msg, bool addLineBreak ) const
{
    std::cout << msg;
    if( addLineBreak ) std::cout << std::endl;
}

// show error message to user (by cerr)
void UserInterface::displayErrorMessage( const std::string& msg ) const
{
    std::cerr << "error: " << msg << std::endl;
}

// sets the error flag for future user notifications
void UserInterface::setProblems()
{
    problems = true;
}

// returns the directory entered by the user
const std::string& UserInterface::getDirectory() const
{
    return directory;
}

// returns the name of file entered by the user (only for one file mode)
const std::string& UserInterface::getFilename() const
{
    return filename;
}

// returns true if single file processing mode is enabled
const bool& UserInterface::isOneFileMode() const
{
    return OneFileMode;
}

// prints the number of records of each type of the passed log
void UserInterface::displayNumberOfEntries( const LogFile& log ) const
{
    std::cout << "TRACE - " <<  log[LogFile::EntryType::TRACE] << std::endl;
    std::cout << "INFO  - " <<  log[LogFile::EntryType::INFO]  << std::endl;
    std::cout << "DEBUG - " <<  log[LogFile::EntryType::DEBUG] << std::endl;
    std::cout << "WARN  - " <<  log[LogFile::EntryType::WARN]  << std::endl;
    std::cout << "ERROR - " <<  log[LogFile::EntryType::ERROR] << std::endl;
}

// prints the general log information
void UserInterface::displayLogInfo( const LogFile& log ) const
{
    std::cout << "*************************************************************************" << std::endl;
    std::cout << "Parsing of file " << log.getFilename() << " results:\n";
    if( log.isCorrect() )
    {
        std::cout << "Date and time of first entry: " << log.getTimeOfFirstEntry() << std::endl;
        std::cout << "Date and time of last entry:  " << log.getTimeOfLastEntry() << std::endl;
        std::cout << "Number of entries of each type:\n";
        displayNumberOfEntries( log );
    }
    else
    {
        std::cout << "This file is incorrect\n";
    }
}

// displaying information about the result of parsing, taking into account the set flags
void UserInterface::displayResult( 
                                  const std::vector<LogFile>& LogList, 
                                  const LogFile& TotalInfo
                                 ) const
{
    if( OneFileMode )
    {
        displayLogInfo(*(LogList.end() - 1));
    }
    else
    {
        // display a list with the names of found log files
        std::cout << "Found "  << LogList.size() << " files: " << std::endl;
        for( auto logfile = LogList.begin(); logfile != LogList.end(); ++logfile )
        {
            std::cout << logfile->getFilename() << std::endl;
        }
        std::cout << std::endl;
        for( auto logfile = LogList.begin(); logfile != LogList.end(); ++logfile )
        {
            displayLogInfo(*logfile);
        }
        // display information about the total number of entries of each type
        std::cout << "\n#########################################################################\n";
        std::cout << "\nTotal number of entries of each type in all files:\n";
        displayNumberOfEntries( TotalInfo );
    }

    // indicate possible problems
    if( problems ) std::cout << "\nFound incorrect file(s): see report above.\n"; 
}
