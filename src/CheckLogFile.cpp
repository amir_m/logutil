/*
    This file contains defenition of method CheckLogFile of class LogParser
*/
#include <string>
#include <vector>
#include "../include/LogParser.h"

/*
* bool LogParser::checkLogFile(const std::vector<char>& buf, uint32_t); 
*
* DESCRIPTION
*   Takes a buffer containing the beginning of a log file 
*   and checks for a file date message at the beginning of this buffer
*
* PARAMETRS
*   const std::vector<char>& buf - buffer link contains THE BEGINNING of the file 
*   uint32_t - buffer size
*
* RETURN VALUE
*   bool - true if the file is valid and false otherwise
*/
bool LogParser::CheckLogFile( const std::vector<char>& buf, uint32_t len )
{

    const std::string tmp = "\n--------------------------------------------------------------------------\nNew run at ";
    const std::string tmpOfData = "####-^^^-## ##:##:##, PID=######";  // template of part with data 
    //              Example:  2021-Oct-07 11:37:21, PID=197096    // (# - digit, ^ - symbol)

    bool correct = true;
    
    uint32_t i = 0, g = 0;
    for( ; g < tmp.length(); i++, g++)
    {
        if( buf[i] == '\r')
        {
            i++;
            continue;
        }
        if( buf[i] != tmp[g] ) 
        {
            correct = false;
            break;
        }
    }

    for( uint32_t j = 0; j < tmpOfData.length(); i++, j++)
    {
        switch ( tmpOfData[j] )
        {
            case '#':
                if( (buf[i] < '0') || (buf[i] > '9') )  // if not digit
                {
                    correct = false;
                }
                break;
            case '^':
                if( ((buf[i] < 'a') || (buf[i] > 'z')) &&
                     ((buf[i] < 'A') || (buf[i] > 'Z') ) )  // if not symbol
                {
                    correct = false;
                }
                break;
            default:
                if( buf[i] != tmpOfData[j] ) 
                {
                    correct = false;
                }
        }
        if( !correct ) break;
    }

    return correct;
} 


