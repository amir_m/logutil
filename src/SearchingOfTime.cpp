/*
*   This file contains the definition of the function SearchingOfTime 
*/
#include <string>
#include <vector>
#include "../include/LogParser.h"

/*
* std::string LogParserSearchingOfTime(const std::vector<char>& buf, uint32_t, uint32_t, std::string& );
*
* DESCRIPTION
*   Finds the first occurrence matching the date and time pattern in the given buffer 
*       and stores it in the given string
*
* PARAMETRS
*   std::vector<char>& - buffer link
*   uint32_t - buffer size
*   uint32_t -  start position
*    
* RETURN_VALUE
*   std::string - string with data and time
*/
std::string LogParser::SearchingOfTime(
                            const std::vector<char>& buf,
                            uint32_t len, 
                            uint32_t position 
                           )
{

    const char DATE_TEMPLATE[] = "xxxx-xx-xx";   // date substring pattern
    const char TIME_TEMPLATE[] = "xx:xx:xx.xxx"; // time substring pattern
    char date[ sizeof( DATE_TEMPLATE ) ];
    char time[ sizeof( TIME_TEMPLATE ) ];
    int dateCurChar = 0;    // points to the current character being tested in the date pattern
    int timeCurChar = 0;    // points to the current character being tested in the time pattern

    for( uint32_t i = position; i < len; i++ )
    {
        // replace the digit character with 'x'
        char ch = (('0' <= buf[i]) && (buf[i] <= '9')) ? 'x' : buf[i];


        // check character against date pattern
        if( ch == DATE_TEMPLATE[dateCurChar] ) 
        {
            date[dateCurChar] = buf[i];
            dateCurChar++;
        }
        else if ( DATE_TEMPLATE[dateCurChar] != '\0' )
        {
            dateCurChar = 0;
        }


        // check character against time pattern
        if( ch == TIME_TEMPLATE[timeCurChar] )
        {
            time[timeCurChar] = buf[i];
            timeCurChar++;
        }
        else if ( TIME_TEMPLATE[dateCurChar] != '\0' )
        {
            timeCurChar = 0;
        }

        // break, if both were found
        if( ( DATE_TEMPLATE[dateCurChar] == '\0' ) && ( TIME_TEMPLATE[timeCurChar] == '\0' ) ) 
        {
            break;
        }


    }

    date[sizeof(DATE_TEMPLATE)-1] = time[sizeof(TIME_TEMPLATE)-1] = '\0';
    return std::string(date) + std::string("; ") + std::string(time);
}